import { FETCH_EXERCISES, NEW_EXERCISE } from "./types";

export const fetchExercises = dispatch({
  type: FETCH_EXERCISES
});

export const newExercise = (exercise) => {
  dispatch({
    type: NEW_EXERCISE,
    payload: exercise,
  });
};
