import { combineReducers } from 'redux';
import { exerciseReducer } from './exerciseReducers';

export default combineReducers({
  exercise: exerciseReducer,
});