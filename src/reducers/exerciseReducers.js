import { FETCH_EXERCISES, NEW_EXERCISE } from '../actions/types';

const initialState = {
  exercises: [],
  exercise: {
    name: "",
    weight: 0,
    numOfRepetitions: 1
  }
};

export const exerciseReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EXERCISES:
      return {
        ...state,
        exercises: action.payload
      };
    case NEW_EXERCISE:
      return {
        ...state,
        exercise: action.payload
      };
    default:
      return state;
  }
}